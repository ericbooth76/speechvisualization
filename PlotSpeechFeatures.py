import argparse
import os
import sys
import numpy as np
import scipy.io.wavfile as wav
from python_speech_features import mfcc
from python_speech_features import fbank
from python_speech_features import logfbank
import matplotlib
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Create and plot data files containing speech features from a mono wav file')
parser.add_argument("--plot", action="store_true", help="turn on plotting")
parser.add_argument("--csv", action="store_true", help="turn on csv file creation")
parser.add_argument("wavfile", help="Input .wav file")
args = parser.parse_args()

numcep=26
#todo - add check for filename
filename=args.wavfile
filebase=args.wavfile.split('.')[0]
outfile_audio=filebase+"-audio.csv"
outfile_spec=filebase+"-spec.csv"
outfile_mfcc=filebase+"-mfcc.csv"

fs, audio = wav.read(args.wavfile)
#deepspeech uses ... sf_mfcc = mfcc(audio, samplerate=fs, numcep=numcep, winlen=0.032, winstep=0.02, winfunc=np.hamming)
sf_mfcc = mfcc(audio, samplerate=fs, numcep=numcep, winlen=0.02, winstep=0.01, winfunc=np.hamming, appendEnergy=False)
(sf_fbank,energies) = fbank(audio, samplerate=fs, winlen=0.02, winstep=0.01, winfunc=np.hamming)
sf_logfbank = logfbank(audio, samplerate=fs, winlen=0.02, winstep=0.01)
sf_mfcc_norm = (sf_mfcc-sf_mfcc.min())/(sf_mfcc.max()-sf_mfcc.min())
sf_fbank_norm = (sf_fbank-sf_fbank.min())/(sf_fbank.max()-sf_fbank.min())
sf_logfbank_norm = (sf_logfbank-sf_logfbank.min())/(sf_logfbank.max()-sf_logfbank.min())

mfcc_samples=sf_mfcc.shape[0]
audio_to_mfcc_scale=3
audio_stride=int(audio.shape[0]/(audio_to_mfcc_scale*sf_mfcc.shape[0]))
audio_resampled=audio[0::audio_stride]

if args.csv:
    a = np.asarray(audio_resampled)
    np.savetxt(outfile_audio, a, delimiter=",", fmt='%6.4f')
    a = np.asarray(sf_mfcc_norm)
    np.savetxt(outfile_mfcc, a, delimiter=",", fmt='%6.4f')
    a = np.asarray(sf_logfbank_norm)
    np.savetxt(outfile_spec, a, delimiter=",", fmt='%6.4f')

if args.plot:
    plt.figure(1)
    plt.subplot(311)
    plt.title('Waveform')
    plt.plot(audio)
    plt.axis([0, audio.size, audio.min(), audio.max()])
    #plt.subplot(311)
    #plt.title('Waveform Resampled')
    #plt.plot(audio_resampled)
    #plt.axis([0, audio_resampled.size, audio_resampled.min(), audio_resampled.max()])
    #plt.subplot(513)
    #plt.title('Spectrogram')
    #plt.imshow(sf_mfcc_norm[1000:3000].transpose(), extent=[-4,4,-1,1])
    #plt.imshow(sf_fbank_norm.transpose(), aspect=audio_to_mfcc_scale)
    #plt.colorbar()
    plt.subplot(312)
    plt.title('Log Spectrogram')
    plt.imshow(sf_logfbank_norm.transpose(), aspect=5)
    plt.subplot(313)
    plt.title('mfcc Spectrogram')
    plt.imshow(sf_mfcc_norm.transpose(), aspect=5)
    plt.show()
    
    #plt.hist(mfcc_norm.transpose().ravel(), bins=256, range=(0.0,1.0), fc='k', ec='k')
    #plt.show()
